package g30126.Ciorba.Alexandru.l4.e5;
import g30126.Ciorba.Alexandru.l4.e4.*;

public class Book {
	String name;
	Author author;
	double price;
	int qtyInStock;
	
	public Book(String name,Author author,double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
	}
	
	Book(String name,Author author,double price,int qtyInStock)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Author getAuthor()
	{
		return this.author;
	}
	
	public Double getPrice()
	{
		return this.price;
	}
	
	public void setPrice(double price)
	{
		this.price=price;
	}
	
	public int getQtyInStock()
	{
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	
	public String toString()
	{
		return "'"+this.name+"'"+" "+this.author.getName()+"("+this.author.getGender()+") at "+this.author.getEmail(); 
	}
}
