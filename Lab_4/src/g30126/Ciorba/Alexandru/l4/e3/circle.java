package g30126.Ciorba.Alexandru.l4.e3;

public class circle {
	private double radius;
	private String color;
	
	public circle()
	{
		this.radius=1.0;
		this.color="red";
	}
	
	public circle(double rad)
	{
		this.radius=rad;
	}
	
	public double getRadius()
	{
		return this.radius;
	}
	
	public double getArea()
	{
		return 3.14*this.radius*this.radius;
	}
}
