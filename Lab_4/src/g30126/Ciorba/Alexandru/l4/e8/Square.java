package g30126.Ciorba.Alexandru.l4.e8;

public class Square extends Rectangle{
	public Square()
	{
		super();
	}
	
	public Square(double side)
	{
		super(side,side);
	}
	
	public Square(double side,String color,boolean filled)
	{
		super(side,side,color,filled);
	}
	
	public double getSide()
	{
		return this.width;
	}
	
	public void setSide(double side)
	{
		this.width=this.length=side;
	}
	
	public void setWidth(double side)
	{
		this.width=this.length=side;
	}
	
	public void setLength(double side)
	{
		this.width=this.length=side;
	}
	
	public String toString()
	{
		return "A Square with side="+this.width+", which is a subclass of "+super.toString();
	}
}
