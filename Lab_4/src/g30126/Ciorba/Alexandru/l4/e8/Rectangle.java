package g30126.Ciorba.Alexandru.l4.e8;

public class Rectangle extends Shape{
	double width=1;
	double length=1;
	public Rectangle()
	{
		super();
		this.width=1;
		this.length=1;
	}
	
	public Rectangle(double width,double length)
	{
		super();
		this.width=width;
		this.length=length;
	}
	
	public Rectangle(double width,double length,String color,boolean filled)
	{
		super(color,filled);
		this.width=width;
		this.length=length;
	}
	
	public double getWidth()
	{
		return this.width;
	}
	
	public void setWidth(double width)
	{
		this.width=width;
	}
	
	public double getLength()
	{
		return this.length;
	}
	
	public void setLength(double length)
	{
		this.length=length;
	}
	
	public double getArea()
	{
		return this.width*this.length;
	}
	
	public double getPerimeter()
	{
		return 2*this.length+2*this.width;
	}
	
	public String toString()
	{
		return "A Rectangle with width="+this.width+" and length="+this.length+", which is a subclass of "+super.toString();
	}
}
