package g30126.Ciorba.Alexandru.l4.e6;
import g30126.Ciorba.Alexandru.l4.e4.Author;

public class Book {
	String name;
	Author[] authors;
	double price;
	int qtyInStock=0;
	
	Book(String name,Author[] authors,double price)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	
	Book(String name,Author[] authors,double price,int qtyInStock)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Author[] getAuthors()
	{
		return this.authors;
	}
	
	public Double getPrice()
	{
		return this.price;
	}
	
	public void setPrice(double price)
	{
		this.price=price;
	}
	
	public int getQtyInStock()
	{
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	
	public void printAuthors()
	{
		
		int i=0;
		for(i=0;i<=this.authors.length;i++)
			System.out.println(this.authors[i]);
	}
	
	public String toString()
	{
		return "'"+this.name+"'"+" "+this.authors.length; 
	}
}
