package g30126.Ciorba.Alexandru.l4.e4;

public class Author {
	String name;
	String email;
	char gender;
	
	public Author(String name,String email,char gender)
	{
		this.name=name;
		this.email=email;
		if(gender=='f' || gender=='m')
			this.gender=gender;
		else
			System.out.println("Gender is wrong");
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getEmail()
	{
		return this.email;
	}
	
	public void setEmail(String email)
	{
		this.email=email;
	}
	
	public char getGender()
	{
		return this.gender;
	}
	
	public String toString()
	{
		return "author-"+this.name+" ("+this.gender+") at "+this.email; 
	}
}
