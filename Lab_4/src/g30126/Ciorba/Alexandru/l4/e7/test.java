package g30126.Ciorba.Alexandru.l4.e7;
import g30126.Ciorba.Alexandru.l4.e3.*;

public class test {

	public static void main(String[] args)
	{
		Cylinder a = new Cylinder();
		Cylinder b = new Cylinder(2);
		Cylinder c = new Cylinder(2,1);
		System.out.println("Inaltimea cilindrului 1:"+a.getRadius());
		System.out.println("Inaltimea cilindrului 2:"+b.getRadius());
		System.out.println("Inaltimea cilindrului 3:"+c.getRadius());
		System.out.println("Volumul cilindrului 1:"+a.getVolume());
		System.out.println("Volumul cilindrului 2:"+b.getVolume());
		System.out.println("Volumul cilindrului 3:"+c.getVolume());
		
	}
}
