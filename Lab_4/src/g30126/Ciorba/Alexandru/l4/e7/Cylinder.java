package g30126.Ciorba.Alexandru.l4.e7;
import g30126.Ciorba.Alexandru.l4.e3.*;

public class Cylinder extends circle {
	private double height; 
	public Cylinder() 
	{
		super();//in loc de circle
		this.height=1;
	}
	
	public Cylinder(double radius)
	{
		super(radius);
		this.height=1;
	}
	
	public Cylinder(double radius,double height)
	{
		super(radius);
		this.height=height;
	}

	public double getHeight() {
		return height;
	}

	public double getVolume()
	{
		return this.height*super.getArea();
	}
}
