package ex1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class TicTacHoe extends JFrame{

	JButton a1,a2,b1,b2;
	
	
	TicTacHoe()
	{
		setTitle("InterfataGrafica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,300);
		initializare();
		setVisible(true);
	}
	
	public void initializare()
	{
		this.setLayout(null);
		a1 = new JButton("1");
		a1.setBounds(20,20,100,25);
		a1.addActionListener(new TratareButon());
		add(a1);
		
		a2 = new JButton("2");
		a2.setBounds(20,40,100,25);
		a2.addActionListener(new TratareButon2());
		add(a2);
		
		b1 = new JButton("3");
		b1.setBounds(20,60,100,25);
		b1.addActionListener(new TratareButon3());
		add(b1);
		
		b2 = new JButton("4");
		b2.setBounds(20,80,100,25);
		b2.addActionListener(new TratareButon4());
		add(b2);
	}
	
	public class TratareButon implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			String a;
			
			try 
			{
				BufferedReader br=new BufferedReader(new FileReader("C:\\Users\\1997a\\Desktop\\date.in.txt"));
				a=br.readLine();
				br.close();
				a1.setText(a);
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}
		}	
	}
	
	public class TratareButon2 implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e1) 
		{
			String a;
			
			try 
			{
				BufferedReader br=new BufferedReader(new FileReader("C:\\Users\\1997a\\Desktop\\date.in.txt"));
				a=br.readLine();
				br.close();
				a2.setText(a);
			}
			catch(IOException e12)
			{
				e12.printStackTrace();
			}
		}	
	}
	
	public class TratareButon3 implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e2) 
		{
			String a;
			
			try 
			{
				BufferedReader br=new BufferedReader(new FileReader("C:\\Users\\1997a\\Desktop\\date.in.txt"));
				a=br.readLine();
				br.close();
				b1.setText(a);
			}
			catch(IOException e22)
			{
				e22.printStackTrace();
			}
		}	
	}
	
	public class TratareButon4 implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e3) 
		{
			String a;
			
			try 
			{
				BufferedReader br=new BufferedReader(new FileReader("C:\\Users\\1997a\\Desktop\\date.in.txt"));
				a=br.readLine();
				br.close();
				b2.setText(a);
			}
			catch(IOException e32)
			{
				e32.printStackTrace();
			}
		}	
	}
	
	public static void main(String args[]) {
		new TicTacHoe();
	}
}
