package g30126.Ciorba.Alexandru.l2.e6;

public class Factorial {
	public int iterativ(int a)
	{
		int i;
		int suma=1;
		for(i=1;i<=a;i++)
			suma=suma*i;
		return suma;
	}
	
	public int recursiv(int a)
	{
		if(a==1)
			return 1;
		else
			return a*recursiv(a-1); 
	}
}
