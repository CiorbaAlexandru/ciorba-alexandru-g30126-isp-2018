package g30126.Ciorba.Alexandru.l2.e6;

import java.util.Scanner;

public class ex6 {
	public static void main(String[] args){
		int a;
		Scanner in = new Scanner(System.in);
		System.out.println("Variabila:");
		a=in.nextInt();
		Factorial fac=new Factorial();
		System.out.println("Suma iterativ:");
		System.out.println(fac.iterativ(a));
		System.out.println("Suma recursiv:");
		System.out.println(fac.recursiv(a));
	}
}
