package g30126.Ciorba.Alexandru.l2.e5;

import java.util.Random;

public class ex5 {
	static void bubbleSort(int[] arr) {  
        int n = arr.length;  
        int temp = 0;  
         for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                          if(arr[j-1] > arr[j]){   
                                 temp = arr[j-1];  
                                 arr[j-1] = arr[j];  
                                 arr[j] = temp;  
                         }  
                          
                 }  
         }  
  
    }  
    public static void main(String[] args) {  
                int arr[] = new int[7];  
                int i;
                Random r = new Random();
            	for(i=0;i<6;i++){
            		arr[i]=r.nextInt(30);
            	}
                 
                System.out.println("Vector inainte de bubble sort:");  
                for(i=0; i < arr.length; i++){  
                        System.out.print(arr[i] + " ");  
                }  
                System.out.println();  
                  
                bubbleSort(arr);//sorting array elements using bubble sort  
                 
                System.out.println("Vector dupa de bubble sort:");  
                for(i=0; i < arr.length; i++){  
                        System.out.print(arr[i] + " ");  
                }  
   
        }  
}

