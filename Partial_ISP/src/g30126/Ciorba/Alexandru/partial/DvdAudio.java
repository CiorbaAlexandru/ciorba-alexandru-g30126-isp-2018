package g30126.Ciorba.Alexandru.partial;

import java.awt.Color;
import java.awt.Graphics;

public class DvdAudio extends Dvd{

	private String title;
	public int tip;
	public int index=-1;
	Color color= Color.RED;
	
	public DvdAudio(String title) 
	{
	       super(title);
	       this.tip=1;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public int getIndex()
	{
		return this.index;
	}
	
	public void setIndex(int index)
	{
		this.index=index;
	}
	
	void play()
	{
		System.out.println("Play dvd audio "+this.title);
	}
	
	public Color getColor()
	{
		return this.color;
	}
	
	public void setColor(Color color)
	{
		this.color=color;
	}
	
	public void draw(Graphics g)
	{
		g.setColor(getColor());
		g.fillRect(400,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(500,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(600,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(700,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(800,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(900,400, 50, 50);
	}
}
