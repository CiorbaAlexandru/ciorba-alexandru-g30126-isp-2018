package g30126.Ciorba.Alexandru.partial;

import java.util.ArrayList;


public interface DvdChanger {

	
		
	   /**
	    * Incarca un dvd in primul slot disponibil din cele 6.
	    * @param dvd
	    * @return
	    */
	   boolean loadDvd(Dvd dvd);

	   /**
	    * Scoate dvd-ul curent din unitate.
	    * @return
	    */
	   Dvd ejectDvd();

	   /**
	    * Schimba dvd-ul curent.
	    *
	    * @param index
	    * @return false daca schimbarea nu s-a realizat cu succes deoarece pe pozitia index nu exista dvd in unitate.
	    */
	   boolean changeDvd(int index);

	   /**
	    * Citeste dvd-ul din unitate de pe pozitia curenta.
	    */
	   void play();

	   /**
	    * Afiseaza detaliile cu privire la dvd-urile incarcate in unitate: titlul dvd-ului si slot-ul (index-ul) unde este incarcat.
	    */
	   void afsieaza();
}