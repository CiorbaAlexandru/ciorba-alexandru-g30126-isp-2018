package g30126.Ciorba.Alexandru.partial;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Dvd {

	   private String title;

	   public Dvd(String title) {
	       this.title = title;
	   }
	   
	   abstract public String getTitle();
	   abstract public int getIndex();
	   abstract public void setIndex(int index);
	   abstract public Color getColor();
	   abstract public void setColor(Color color);
	   public abstract void draw(Graphics g);
	   abstract void play();
	}
