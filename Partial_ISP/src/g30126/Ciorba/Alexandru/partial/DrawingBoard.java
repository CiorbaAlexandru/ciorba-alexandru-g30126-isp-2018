package g30126.Ciorba.Alexandru.partial;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    Dvd[] dvduri = new Dvd[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(1000,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Dvd s1){
        for(int i=0;i<dvduri.length;i++){
            if(dvduri[i]==null){
                dvduri[i] = s1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<dvduri.length;i++){
            if(dvduri[i]!=null)
                dvduri[i].draw(g);
        }
    }

}
