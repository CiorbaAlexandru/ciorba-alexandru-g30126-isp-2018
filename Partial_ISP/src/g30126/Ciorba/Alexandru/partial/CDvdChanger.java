package g30126.Ciorba.Alexandru.partial;

import java.awt.Color;
import java.util.ArrayList;

public class CDvdChanger implements DvdChanger{

		ArrayList<Dvd> dvduri = new ArrayList<>(6);
		static int i=1;
	
	/**
	    * Incarca un dvd in primul slot disponibil din cele 6.
	    * @param dvd
	    * @return
	    */
		public CDvdChanger()
		{
			dvduri = new ArrayList();
		}
		
	   public boolean loadDvd(Dvd dvd)
	   {
		   if(i==6)
			   return false;
		   else
		   {
			   dvduri.add(dvd);
			   dvd.setIndex(i);
			   Color color=Color.GREEN;
			   dvd.setColor(color);
			   i++;
			   return true;
		   }
	   }

	   /**
	    * Scoate dvd-ul curent din unitate.
	    * @return
	    */
	   public Dvd ejectDvd()
	   {
		   for(Dvd d:dvduri)
		   {
			   if(d.getIndex()==i)
			   {
				   dvduri.remove(d);
				   i--;
				   Color color=Color.RED;
				   return d;
			   }
			   
		   }
		return null;
	   }

	   /**
	    * Schimba dvd-ul curent.
	    *
	    * @param index
	    * @return false daca schimbarea nu s-a realizat cu succes deoarece pe pozitia index nu exista dvd in unitate.
	    */
	   public boolean changeDvd(int index)
	   {
			Dvd value1 = dvduri.get(index);
			if(value1==null)
			{
				return false;
			}
			else
			{
				Dvd value2 = dvduri.get(i);
				Dvd value3;
				value3=value1;
				value1=value2;
				value2=value3;
				return true;
			}
			
	   }

	   /**
	    * Citeste dvd-ul din unitate de pe pozitia curenta.
	    */
	   public void play()
	   {
		   for(Dvd d:dvduri)
		   {
			   if(d.getIndex()==i)
			   {
				   d.play();
			   }
		   }
	   }

	   /**
	    * Afiseaza detaliile cu privire la dvd-urile incarcate in unitate: titlul dvd-ului si slot-ul (index-ul) unde este incarcat.
	    */
	   public void afsieaza()
	   {
		   for(Dvd d:dvduri) 
			{
				System.out.println("Titlu DVD:"+d.getTitle()+", indexul DVD:"+d.getIndex());
			}
		   
	   }
	   
	   
}
