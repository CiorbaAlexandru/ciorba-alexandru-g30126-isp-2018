package g30126.Ciorba.Alexandru.partial;

import java.awt.Color;
import java.awt.Graphics;

public class DvdVideo extends Dvd{

	private String title;
	public int tip;
	public int index=-1;
	Color color= Color.RED;
	
	public DvdVideo(String title) 
	{
	       super(title);
	       this.tip=2;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public int getIndex()
	{
		return this.index;
	}
	
	public void setIndex(int index)
	{
		this.index=index;
	}
	
	void play()
	{
		System.out.println("Play dvd video "+this.title);
	}
	
	public Color getColor()
	{
		return this.color;
	}
	
	public void setColor(Color color)
	{
		this.color=color;
	}
	
	public void draw(Graphics g)
	{
		g.setColor(getColor());
		g.fillRect(400,400, 50, 50);
		g.setColor(getColor());
		g.fillRect(500,500, 50, 50);
		g.setColor(getColor());
		g.fillRect(600,600, 50, 50);
		g.setColor(getColor());
		g.fillRect(700,700, 50, 50);
		g.setColor(getColor());
		g.fillRect(800,800, 50, 50);
		g.setColor(getColor());
		g.fillRect(900,900, 50, 50);
	}
}
