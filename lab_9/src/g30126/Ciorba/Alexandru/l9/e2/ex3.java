package g30126.Ciorba.Alexandru.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;


public class ex3 extends JFrame{

	JButton b; //buton
	JTextField count; //text field
	JLabel nr; //eticheta
	int k=0;
	
	ex3(){
		
        setTitle("Press button to inc");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
	}
	
	 public void init(){
		 
         this.setLayout(null);
         int width=80;int height = 20;
         
         nr = new JLabel("Number ");
         nr.setBounds(10, 50, width, height);
         
         count = new JTextField(String.valueOf(k));
         count.setBounds(70,50,width, height);
         
         b = new JButton("INC");
         b.setBounds(10,150,width, height);
         
         b.addActionListener(new TratareButon());
         
         add(nr);add(count);add(b);
	 }
	 
	 
	 public class TratareButon implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			k++;
			ex3.this.count.setText(String.valueOf(k));
		}	
	 }
	 
	 
	 public static void main(String[] args) {
        new ex3();
   }

}

