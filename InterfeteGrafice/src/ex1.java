import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class ex1 extends JFrame{
	
	JButton buton;
	JTextField text;
	String continut;
	
	ex1()
	{
		setTitle("InterfataGrafica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,300);
		initializare();
		setVisible(true);
	}
	
	public void initializare()
	{
		this.setLayout(null);
		buton = new JButton("Buton");
		buton.setBounds(40,40,100,25);
		buton.addActionListener(new TratareButon());
		add(buton);
		
		text=new JTextField("Ana are mere.");
		text.setBounds(40,80,100,100);
		add(text);
	}
	
	public class TratareButon implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			continut=text.getText();
			System.out.println(continut);
			try 
			{
				BufferedWriter bw=new BufferedWriter(new FileWriter("C:\\Users\\1997a\\Desktop\\date.in.txt"));
				bw.write(continut);
				bw.flush();
			}
			catch(IOException e1)
			{
				e1.printStackTrace();
			}
		}	
	}
	
	public static void main(String[] args)
	{
		//new ex1();
		/*
		Scanner in = new Scanner(System.in);
		int a;
		a=in.nextInt();
		in.close();
		System.out.println(a);
		*/
		/*
		Random rnd = new Random(100);
		int b;
		for(int i=0;i<10;i++)
		{
			b=rnd.nextInt(10);
			System.out.println(b);
		}
		*/
		/*
		Scanner in = new Scanner(System.in);
		String a;
		a=in.next();
		in.close();
		System.out.println(a);
		*/
	}
}
