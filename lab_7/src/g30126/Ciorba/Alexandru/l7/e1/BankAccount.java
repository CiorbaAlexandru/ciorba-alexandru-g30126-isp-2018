package g30126.Ciorba.Alexandru.l7.e1;

public class BankAccount {
	
	private String owner;
	private double balance;
	
	public void withdraw(double amount)
	{
		double b=getBalance();
		if(b<amount){
			System.out.println("Not enough balance");
			System.exit(0);
		}
		else{
			setBalance(b-amount);
		}
	}
	
	public void deposit(double amount)
	{
		double b=getBalance();
		setBalance(b+amount);
	}
	
	public String getOwner()
	{
		return owner;
	}

	public double getBalance() 
	{
		return balance;
	}

	public void setBalance(double balance) 
	{
		this.balance = balance;
	}

	public BankAccount(String owner, double balance) 
	{
		this.owner = owner;
		this.balance = balance;
	}

	@Override
	public boolean equals(Object obj) {
		BankAccount b = (BankAccount) obj;
		if (this == b)
			return true;
		if (b == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}
	
}
