package g30126.Ciorba.Alexandru.l7.e2;

public class Main {
	public static void main(String args[])
	{
		BankAccount a = new BankAccount("Sorin",4000);
		BankAccount b = new BankAccount("Liviu",2000);
		BankAccount c = new BankAccount("Stefan",3000);	
		Bank banca=new Bank();
		banca.addAccount(a.getOwner(),a.getBalance());
		banca.addAccount(b.getOwner(),b.getBalance());
		banca.addAccount(c.getOwner(),c.getBalance());
		banca.printAccounts();
	}

}
