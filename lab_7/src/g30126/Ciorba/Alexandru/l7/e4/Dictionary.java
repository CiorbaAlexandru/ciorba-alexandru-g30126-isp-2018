package g30126.Ciorba.Alexandru.l7.e4;

import java.util.ArrayList;
import java.util.HashMap;

import g30126.Ciorba.Alexandru.l7.e2.BankAccount;

public class Dictionary {

	private HashMap<Word,Definition> cuvinte;
	
	public Dictionary()
	{
		cuvinte = new HashMap<>();
	}
	
	public void addWord(Word w,Definition d)
	{
		cuvinte.put(w,d);
	}
	
	public Definition getDefinition(Word w)
	{
		return cuvinte.get(w);
	}
	
	public void getAllWords()
	{
		for(Word w:cuvinte.keySet()) {
			System.out.println(w);
		}
	}
	
	public void getAllDefinitions()
	{
		for(Definition d:cuvinte.values())
		{
			System.out.println(d);
		}
	}
}
