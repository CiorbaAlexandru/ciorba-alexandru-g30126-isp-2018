package g30126.Ciorba.Alexandru.l7.e3;

public class BankAccount implements Comparable{
	
	private String owner;
	private double balance;
	
	public void withdraw(double amount)
	{
		double b=getBalance();
		if(b<amount){
			System.out.println("Not enough balance");
			System.exit(0);
		}
		else{
			setBalance(b-amount);
		}
	}
	
	public void deposit(double amount)
	{
		double b=getBalance();
		setBalance(b+amount);
	}
	
	public String getOwner()
	{
		return owner;
	}

	public double getBalance() 
	{
		return balance;
	}

	public void setBalance(double balance) 
	{
		this.balance = balance;
	}

	public BankAccount(String owner, double balance) 
	{
		this.owner = owner;
		this.balance = balance;
	}

	@Override
    public int compareTo(Object o)
	{
        BankAccount b = (BankAccount) o;
        if(balance>b.getBalance()) return 1;
        if(balance==b.getBalance()) return 0;
        return -1;
    }
}
