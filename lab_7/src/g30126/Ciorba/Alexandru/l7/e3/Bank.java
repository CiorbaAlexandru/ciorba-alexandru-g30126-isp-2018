package g30126.Ciorba.Alexandru.l7.e3;

import java.util.Collections;
import java.util.TreeSet;

import g30126.Ciorba.Alexandru.l7.e2.BankAccount;

public class Bank {

	private TreeSet <BankAccount> accounts;
	
	public Bank()
	{
		accounts = new TreeSet<>();
	}
	
	public void addAccount(String owner,double balance)
	{
		BankAccount b = new BankAccount(owner,balance);
        accounts.add(b);
	}
	
	public void printAccounts()
	{
		
		for(BankAccount a:accounts)
		{
			System.out.println(a.getOwner()+" "+a.getBalance());	
		}
	}
	
	public void printAccounts(double minBalance,double maxBalance)
	{
		for(BankAccount b:accounts)
		{
			if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
				System.out.println(this.accounts);
		}
	}
	
	public BankAccount getAccount(String owner)
	{
		for(BankAccount b:accounts)
		{
			if(b.getOwner()==owner)
				return b;
		}
		return null;
	}
	
	public void getAllAccount()
	{
		String nume;
		for(BankAccount a:accounts)
		{
			nume=a.getOwner();
			for(BankAccount b:accounts)
			{
				if(a.getOwner().compareTo(b.getOwner())<0)
				{
					nume=a.getOwner();
				}
			}
			System.out.println(getAccount(nume));	
		}
	}
}
