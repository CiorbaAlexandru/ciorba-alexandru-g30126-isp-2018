package g30126.Ciorba.Alexandru.l8.e4;

public class Main {
	public static void main(String[] args) throws Exception{
		
		CarManager cm = new CarManager();
		
		Car car1 = cm.createCar("Dacia",8000);
		Car car2 = cm.createCar("Golf",10000);
		cm.addCar(car1, "car1.dat");
		cm.addCar(car2, "car2.dat");
		
		Car car3 = cm.createCar("Aro",65000);
		cm.addCar(car3, "car3.dat");
		System.out.println(car1.toString());
	}
}