package g30126.Ciorba.Alexandru.l8.e4;

import java.io.*;

public class Car implements Serializable{

	public String model;
	public int price;
	
	public Car(String model, int price){
		this.model=model;
		this.price=price;
	}
	
	public String getModel() {
		return model;
	}
	public double getPrice() {
		return price;
	}
	public String toString(){
		return getModel() + " - " + getPrice();
	}
}

