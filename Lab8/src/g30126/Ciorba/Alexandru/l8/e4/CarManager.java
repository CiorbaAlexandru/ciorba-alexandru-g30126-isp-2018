package g30126.Ciorba.Alexandru.l8.e4;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class CarManager {

	public CarManager(){}
	
	Car createCar(String model, int price){
		Car c = new Car(model,price);
		return c;
	
	}
	
	void addCar(Car car,String s) throws IOException{
		ObjectOutputStream oStream = new ObjectOutputStream(new FileOutputStream(s));
		oStream.writeObject(car);
		System.out.println("Scris in fisier");
	}
	
	Car delCar(String s) throws IOException, ClassNotFoundException{
		ObjectInputStream iStream=new ObjectInputStream(new FileInputStream(s));
		Car car=(Car)iStream.readObject();
		System.out.println("Citit din fisier");
		return car;
	}
	
	
}