package g30126.Ciorba.Alexandru.l8.e1;

public class ObjectException extends Exception{

	int o;
	public ObjectException(int o,String msg) 
	{
        super(msg);
        this.o = o;
	}
	
	int getObj()
	{
        return o;
	}
}
