package g30126.Ciorba.Alexandru.l8.e1;

class Cofee{
    private int temp;
    private int conc;
    private int obj;

    Cofee(int t,int c){temp = t;conc = c;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getObj(){return obj;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}
