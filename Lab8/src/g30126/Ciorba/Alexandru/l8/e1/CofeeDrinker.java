package g30126.Ciorba.Alexandru.l8.e1;

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, ObjectException{
          if(c.getTemp()>60)
                throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
          if(c.getConc()>50)
                throw new ConcentrationException(c.getConc(),"Cofee concentration to high!"); 
          if(c.getObj()>3)
        	  	throw new ObjectException(c.getObj(),"Too many cofee cups!");
          System.out.println("Drink cofee:"+c);
    }
}