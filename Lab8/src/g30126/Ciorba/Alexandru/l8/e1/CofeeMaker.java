package g30126.Ciorba.Alexandru.l8.e1;

class CofeeMaker {
	int count=0;
    Cofee makeCofee() throws ObjectException{
          System.out.println("Make a coffe");
          int t = (int)(Math.random()*100);
          int c = (int)(Math.random()*100);
          count ++;
          if(count>5)
      	  	throw new ObjectException(c, "Too many cofee cups!");
          Cofee cofee = new Cofee(t,c);
          return cofee;
    }

}