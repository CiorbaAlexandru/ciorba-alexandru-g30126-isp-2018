import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class Alex2019 extends JFrame{
	
	JButton buton;
	JTextField text;
	String continut;
	
	Alex2019()
	{
		setTitle("InterfataGrafica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,300);
		initializare();
		setVisible(true);
	}
	
	public void initializare()
	{
		this.setLayout(null);
		buton = new JButton("Buton");
		buton.setBounds(40,40,100,25);
		buton.addActionListener(new TratareButon());
		add(buton);
		
		text=new JTextField("Ana are mere.");
		text.setBounds(40,80,100,100);
		add(text);
	}
	
	
	
	
	public static void main(String[] args)
	{
		new Alex2019();	
	}
}