package g30126.Ciorba.Alexandru.examen;

import java.awt.Graphics;

public interface Produs {

	int getPret();
	int getCantitate();
	String getNume();
	void setCantitate(int cantitate);   
	public abstract void draw(Graphics g);
}
