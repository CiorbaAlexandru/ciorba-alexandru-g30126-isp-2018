package g30126.Ciorba.Alexandru.examen;

import java.util.ArrayList;
import java.util.Iterator;

public class StocManager implements IStocManager{

	ArrayList<Produs> produse = new ArrayList<>();
	
	public StocManager()
	{
		produse = new ArrayList();
	}
	
	public ArrayList<Produs> getProduse()
	{
		return this.produse;
	}
	
	public void adaugaProdus(Produs p)
	{
		produse.add(p);
	}
	
	public void stergeProdus(String numeProdus)
	{
		for(Produs p:produse) 
		{
			if(p.getNume().equals(numeProdus))
			{
				produse.remove(p);
			}
		}
		/*
		Iterator<Produs> i = produse.iterator();
        while(i.hasNext()){
            Produs prod = i.next();
            if(prod.getNume().equalsIgnoreCase(numeProdus))
                i.remove();
        }
        */
	}
	   
	public void afiseazaValoareStoc()
	{
		int sum=0;
		for(Produs p:produse) 
		{
			sum=sum+p.getPret()*p.getCantitate();
		}
		System.out.println("Valoare totala a stocului este "+sum);
	}
	   
	public void afiseazaProdusCantitateMinima(int min)
	{
		for(Produs p:produse) 
		{
			if(p.getCantitate()==min)
				System.out.println(p);
		}
	}

	public void incrementeazaStocProdus(String numeProdus)
	{
		for(Produs p:produse) 
		{
			if(p.getNume()==numeProdus)
			{
				p.setCantitate(p.getCantitate()+1);
			}
		}
	}
   
	public void decrementeazaStocProdus(String numeProdus)
	{
		for(Produs p:produse) 
		{
			if(p.getNume()==numeProdus)
			{
				p.setCantitate(p.getCantitate()-1);
			}
		}
	}
}
