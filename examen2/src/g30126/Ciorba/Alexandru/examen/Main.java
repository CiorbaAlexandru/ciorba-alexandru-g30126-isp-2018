package g30126.Ciorba.Alexandru.examen;

public class Main {

	public static void main(String[] args)
	{
		Produs p1 = new CProdus(500,25,100,"Pizza");
		Produs p2 = new CProdus(600,5,50,"Suc");
		Produs p3 = new CProdus(700,12,200,"Kebab");
		StocManager sm = new StocManager();
		sm.adaugaProdus(p1);
		sm.adaugaProdus(p2);
		sm.adaugaProdus(p3);
		
		DrawingBoard b1 = new DrawingBoard();
		b1.addShape(p1);
		b1.addShape(p2);
	    b1.addShape(p3);
		
		sm.stergeProdus("Suc");
		
		sm.afiseazaProdusCantitateMinima(100);
		sm.afiseazaProdusCantitateMinima(50);
		sm.afiseazaProdusCantitateMinima(200);
		
		sm.afiseazaValoareStoc();
		
		sm.incrementeazaStocProdus("Pizza");
		
		sm.afiseazaValoareStoc();
		
	}
}
