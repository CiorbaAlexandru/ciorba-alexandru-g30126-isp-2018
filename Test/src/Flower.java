import java.util.ArrayList;

public class Flower  implements Comparable {
    private String name;
    private int petals;

    public Flower(String name, int petals) {
        this.name = name;
        this.petals = petals;
    }

    public int getPetals() {
        return petals;
    }

    public String getName() {
        return name;
    }


    @Override
    public int compareTo(Object o) {
        Flower f = (Flower) o;
        if(petals>f.getPetals()) return 1;
        if(petals==f.getPetals()) return 0;
        return -1;
    }


}

