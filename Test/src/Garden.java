import java.util.ArrayList;
import java.util.Collections;

public class Garden {
    private ArrayList<Flower> flowers = new ArrayList<>(3);

    public void plantFlower(Flower f)
    {
        flowers.add(f);
    }
    public void findFlower(String name) {
        for (Flower f:flowers) {
        {
            if (f.getName().equals(name))
                System.out.println("flower " + f.getName() + " " + f.getPetals());
        }
        }
    }
    public void printFlowers()
    {
        Collections.sort(flowers);
        for(Flower f:flowers)
        {
        	System.out.println(f.getName()+" "+f.getPetals());
        }
    }

    public static void main(String[] args) {
        Garden g = new Garden();
        g.plantFlower(new Flower("rose",23));
        g.plantFlower(new Flower("lily",7));
        g.plantFlower(new Flower("tulip",5));
        g.findFlower("lily");
        g.printFlowers();
    }
}