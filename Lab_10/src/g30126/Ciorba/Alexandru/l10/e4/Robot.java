package g30126.Ciorba.Alexandru.l10.e4;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Robot{

	private static class Rob extends Thread
	{
		private int x=0;
		private int y=0;
		private boolean crashed=false;
		
	public Rob(String name,int x,int y){
		super(name);
		this.x=x;
		this.y=y;
	}
	
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public void setCrashed()
	{
		this.crashed=true;
	}
	
	 public void run(){
         for(int i=0;i<100 && crashed==false;i++)
         {
        	int poz = ThreadLocalRandom.current().nextInt(1, 5);
	     	switch(poz)
	     	{
		     	case 1:
		     		if((x+1)<=100)
		     			x=x+1;
		     		break;
		     	case 2:
		     		if((y+1)<=100)
		     			y=y+1;
		     		break;
		     	case 3:
		     		if((x-1)>=0)
		     			x=x-1;
		     		break;
		     	case 4:
		     		if((y-1)>=0)
		     			y=y-1;
		     		break; 	
	     	}
     	
	     	if(crashed==false)
	     	{
              System.out.println(getName() + " x = "+x+ ", y = "+y);
               try {
                     Thread.sleep(100);
               } catch (InterruptedException e) {
                     e.printStackTrace();
               }
	     	}
         }
	 }
	}
	 
	 
	 public static void main(String[] args) {
	        Rob r1 = new Rob("Robot1",0,0);
	        Rob r2 = new Rob("Robot2",5,5);
	        Rob r3 = new Rob("Robot3",10,10);
	        Rob r4 = new Rob("Robot4",15,15);
	        Rob r5 = new Rob("Robot5",20,20);
	        Rob r6 = new Rob("Robot6",25,25);
	        Rob r7 = new Rob("Robot7",30,30);
	        Rob r8 = new Rob("Robot8",35,35);
	        Rob r9 = new Rob("Robot9",40,40);
	        Rob r10 = new Rob("Robot0",45,45);
	        
	        r1.start();
	        r2.start();
	        r3.start();
	        r4.start();
	        r5.start();
	        r6.start();
	        r7.start();
	        r8.start();
	        r9.start();
	        r10.start();
	        
	        while(true)
	        {
	        	if(r1.getX()==r2.getX()&& r1.getY()==r2.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r2.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r3.getX()&& r1.getY()==r3.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r3.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r4.getX()&& r1.getY()==r4.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r4.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r5.getX()&& r1.getY()==r5.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r5.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r6.getX()&& r1.getY()==r6.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r6.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r7.getX()&& r1.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r8.getX()&& r1.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r9.getX()&& r1.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r1.getX()==r10.getX()&& r1.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r1.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r3.getX()&& r2.getY()==r3.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r3.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r4.getX()&& r2.getY()==r4.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r4.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r5.getX()&& r2.getY()==r5.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r5.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r6.getX()&& r2.getY()==r6.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r6.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r7.getX()&& r2.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r8.getX()&& r2.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r9.getX()&& r2.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r2.getX()==r10.getX()&& r2.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r2.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r4.getX()&& r3.getY()==r4.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r4.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r5.getX()&& r3.getY()==r5.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r5.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r6.getX()&& r3.getY()==r6.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r6.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r7.getX()&& r3.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r8.getX()&& r3.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r9.getX()&& r3.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r3.getX()==r10.getX()&& r3.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r3.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r5.getX()&& r4.getY()==r5.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r5.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r6.getX()&& r4.getY()==r6.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r6.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r7.getX()&& r4.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r8.getX()&& r4.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r9.getX()&& r4.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r4.getX()==r10.getX()&& r4.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r4.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r5.getX()==r6.getX()&& r5.getY()==r6.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r5.setCrashed();
	        		r6.setCrashed();
	        		break;
	        	}
	        	if(r5.getX()==r7.getX()&& r5.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r5.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r5.getX()==r8.getX()&& r5.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r5.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r5.getX()==r9.getX()&& r5.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r5.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r5.getX()==r10.getX()&& r5.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r5.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r6.getX()==r7.getX()&& r6.getY()==r7.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r6.setCrashed();
	        		r7.setCrashed();
	        		break;
	        	}
	        	if(r6.getX()==r8.getX()&& r6.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r6.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r6.getX()==r9.getX()&& r6.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r6.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r6.getX()==r10.getX()&& r6.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r6.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r7.getX()==r8.getX()&& r7.getY()==r8.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r7.setCrashed();
	        		r8.setCrashed();
	        		break;
	        	}
	        	if(r7.getX()==r9.getX()&& r7.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r7.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r7.getX()==r10.getX()&& r7.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r7.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r8.getX()==r9.getX()&& r8.getY()==r9.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r8.setCrashed();
	        		r9.setCrashed();
	        		break;
	        	}
	        	if(r8.getX()==r10.getX()&& r8.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r8.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        	if(r9.getX()==r10.getX()&& r9.getY()==r10.getY())
	        	{
	        		System.out.println("Robotii s-au ciocnit");
	        		r9.setCrashed();
	        		r10.setCrashed();
	        		break;
	        	}
	        }
	        
	  	}	
	
}