package g30126.Ciorba.Alexandru.l10.e1;

public class counter extends Thread {
	 
    counter(String name){
          super(name);
    }

    public void run(){
          for(int i=0;i<20;i++){
                System.out.println(getName() + " i = "+i);
                try {
                      Thread.sleep((int)(Math.random() * 1000));
                } catch (InterruptedException e) {
                      e.printStackTrace();
                }
          }
          System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
          counter c1 = new counter("counter1");
          counter c2 = new counter("counter2");
          counter c3 = new counter("counter3");

          c1.run();
          c2.run();
          c3.run();
          
    }
}
