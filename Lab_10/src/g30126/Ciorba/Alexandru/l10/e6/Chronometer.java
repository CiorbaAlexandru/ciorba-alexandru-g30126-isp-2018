package g30126.Ciorba.Alexandru.l10.e6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class Chronometer extends JFrame{

	private Timp t=new Timp();
	
	Chronometer(Timp t)
	{	
		this.t=t;
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,200);
        setVisible(true);
	}
	
	JButton a;
	JButton b;
	JTextField count;
	JLabel nr;
	
	 public void init(){
		 
         this.setLayout(null);
         int width=80;int height = 20;
         
         nr = new JLabel("Timp:");
         nr.setBounds(10, 50, width, height);
         
         count = new JTextField();
         count.setBounds(50,50,width, height);
         
         a = new JButton("Start/Stop");
         a.setBounds(10,100,width*2, height);
         
         b = new JButton("Reset");
         b.setBounds(10,125,width*2, height);
         
         a.addActionListener(new TratareButon1());
         b.addActionListener(new TratareButon());
         
         add(nr);add(count);add(a);add(b);
	 }
	 
	 public class TratareButon1 implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				if(t.getGo()==true)
				{
					t.stopceas();
				}
				else
				{
					t.startceas();
				}

			}	
		 }
		 
	 
	 public class TratareButon implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			t.resetT();
		}	
	 }
	 
	 private static class Time extends Thread
		{
			private Timp t;
			
			public Time(String name,Timp t){
				super(name);
				this.t=t;
			}
			
			 public void run()
			 {
				 while(true) {
					 t.increment();
					 try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				 }
			 }
		}
	 
	 public static void main(String[] args) 
	 {

        Timp ti=new Timp();
        new Time("Ceas",ti).start();
        Chronometer ceas=new Chronometer(ti);
        while(true) {
        	ceas.count.setText(String.valueOf(ti.getT()));
        }
	 }

}
