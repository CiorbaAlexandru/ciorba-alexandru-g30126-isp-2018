package g30126.Ciorba.Alexandru.l10.e3;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Count{
	private static final Lock mtx=new ReentrantLock(true);
	static int nr=0;

    
	private static class Counter extends Thread{
		
	    public Counter(String n)
	    {
	    	super(n);
	    }
	    
		public void run(){
			System.out.println("Firul "+getName()+" a intrat in metoda run()");
			try {
			
				mtx.lock();
				for(int i=0;i<100;i++){
					nr++;
					System.out.println(getName() + " nr = "+nr);
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				mtx.unlock();
			}
		  }
	}
	
  public static void main(String[] args) {
        
        Counter c1=new Counter("c1");
        Counter c2=new Counter("c2");
        c1.start();
        c2.start();
  	}	
}

