package g30126.Ciorba.Alexandru.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color,boolean filled,String id, int length, int width) {
        super(color,filled,id);
        this.length = length;
        this.width = width;
    }
    
    public Rectangle(Color color,boolean filled,String id,int x, int y, int length, int width) {
        super(color,filled,id,x,y);
        this.length = length;
        this.width = width;
    }
    
    public int getLength()
    {
    	return this.length;
    }
    
    public int getWidth()
    {
    	return this.width;
    }

    @Override
    public void draw(Graphics g) {
    	if(isFilled()==false)
    	{
    		System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
    		g.setColor(getColor());
    		g.drawRect(getX(),getY(),width,length);
    	}
    	else
    	{
    		System.out.println("Drawing a filled rectangel "+length+" "+getColor().toString());
    		g.setColor(getColor());
    		g.fillRect(getX(),getY(),width,length);
    	}
    }
}