package g30126.Ciorba.Alexandru.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x=50,y=50;
    private boolean filled;
    private String id;

    public Shape(Color color,boolean filled,String id) {
        this.color = color;
        this.filled=filled;
        this.id=id;
    }
    
    public Shape(Color color,boolean filled,String id,int x,int y)
    {
    	this.filled=filled;
    	this.color = color;
    	this.id=id;
    	this.x=x;
    	this.y=y;
    }

    public Color getColor() {
        return color;
    }
    
    public int getX()
    {
    	return x;
    }
    
    public int getY()
    {
    	return y;
    }
    
    public boolean isFilled()
    {
    	return this.filled;
    }
    
    public String getId()
    {
    	return id;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
    
}
