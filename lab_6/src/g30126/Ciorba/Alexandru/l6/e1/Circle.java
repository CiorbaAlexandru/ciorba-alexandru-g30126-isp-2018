package g30126.Ciorba.Alexandru.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color,boolean filled,String id, int radius) {
        super(color,filled,id);
        this.radius = radius;
    }
    
    public Circle(Color color,boolean filled,String id, int x, int y, int radius) {
        super(color,filled,id,x,y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
    	if(isFilled()==false)
    	{
	        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
	        g.setColor(getColor());
	        g.drawOval(getX(),getY(),radius,radius);
    	}
    	else
    	{
    		System.out.println("Drawing a filled circle "+this.radius+" "+getColor().toString());
	        g.setColor(getColor());
	        g.fillOval(getX(),getY(),radius,radius);
    	}
    }
    
}
