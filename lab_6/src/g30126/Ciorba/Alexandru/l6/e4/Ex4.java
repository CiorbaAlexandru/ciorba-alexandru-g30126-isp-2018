package g30126.Ciorba.Alexandru.l6.e4;

import java.lang.*;

public class Ex4 implements CharSequence {

	public char[] chs = {'a','b','c','5','8','d','g'};
	
	public int length(){
		return chs.length;
	}
	
	public char charAt(int index){
		try{
				return chs[index];
			
		}
		 catch(IndexOutOfBoundsException e){
			return 'e';
		}	
	}

	
	public CharSequence	subSequence(int start, int end){		
		CharSequence csq = "";
					try {
						for(int i = start; i <= end-1; i++){
							if(start < end && end < length())
						csq = csq +""+ chs[i];
						}
					} catch(IndexOutOfBoundsException e){
						
					}
					return csq;	
	}
	
	@Override
	public String toString(){
		String str = "";
		for(int i = 0; i<length();i++){
		str += chs[i];
		}
		return str;
	}



public static void main(String[] args){
	CharSequence csq = new Ex4();
	System.out.println(csq.length());
	System.out.println(csq.charAt(7));
	System.out.println(csq.toString());
	System.out.println(csq.subSequence(2,4));
}
}
