package g30126.Ciorba.Alexandru.l6.e4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Test {
	
	      @Test
		    public void Testlength(){
			 CharSequence csq = new Ex4();    
			 assertEquals("7",csq.length());;
		    }
		 @Test
		    public void TestcharAt(){
			 CharSequence csq = new Ex4();     
			 assertEquals("d",csq.charAt(5));;
		    }
		 @Test
		    public void TestsubSequence(){
			 CharSequence csq = new Ex4();    
			 assertEquals("c5",csq.subSequence(2,4));;
		    }
		 @Test
		    public void TesttoString(){
			 CharSequence csq = new Ex4();     
			 assertEquals("abc58dg",csq.toString());;
		    }
	}
