package g30126.Ciorba.Alexandru.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{

	private Color color=Color.RED;
	private boolean filled=false;
	private String id;
	private int x=50,y=50;
    private int radius;

    public Circle(Color color,boolean filled,String id, int radius) {
        this.color=color;
        this.filled=filled;
        this.id=id;
        this.radius = radius;
    }
    
    public Circle(Color color,boolean filled,String id, int x, int y, int radius) {
        this.color=color;
        this.filled=filled;
        this.id=id;
        this.radius = radius;
        this.x=x;
        this.y=y;
    }

    public Color getColor()
    {
    	return this.color;
    }
    
    public int getX()
    {
    	return this.x;
    }
    
    public int getY()
    {
    	return this.y;
    }
    
    public boolean isFilled()
    {
    	return this.filled;
    }
    
    public String getId()
    {
    	return this.id;
    }

    public void setColor(Color color)
    {
    	this.color=color;
    }
    
    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
    	if(isFilled()==false)
    	{
	        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
	        g.setColor(getColor());
	        g.drawOval(getX(),getY(),radius,radius);
    	}
    	else
    	{
    		System.out.println("Drawing a filled circle "+this.radius+" "+getColor().toString());
	        g.setColor(getColor());
	        g.fillOval(getX(),getY(),radius,radius);
    	}
    }
    
}