package g30126.Ciorba.Alexandru.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
    
    Color getColor();
    
    public int getX();
    
    public int getY();
    
    boolean isFilled();
    
    String getId();

    void setColor(Color color);

    public abstract void draw(Graphics g);
    
}

