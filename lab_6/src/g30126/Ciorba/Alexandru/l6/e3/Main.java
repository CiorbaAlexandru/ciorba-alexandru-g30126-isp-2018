package g30126.Ciorba.Alexandru.l6.e3;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,true,"Circle1",500,500,90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,false,"Circle2",500,500,150);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,false,"Rectangle1",500,500,100,100);
        b1.addShape(s3);
        Shape s4 = new Rectangle(Color.YELLOW,false,"Rectangle2",500,500,150,150);
        b1.addShape(s4);
        b1.deleteById("Circle2");

    }
}
