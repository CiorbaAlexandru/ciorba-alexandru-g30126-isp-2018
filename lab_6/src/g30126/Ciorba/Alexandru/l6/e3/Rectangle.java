package g30126.Ciorba.Alexandru.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape{

    private int length;
    private int width;
    private Color color=Color.RED;
	private boolean filled=false;
	private String id;
	private int x=50,y=50;

    public Rectangle(Color color,boolean filled,String id, int length, int width) {
        this.color=color;
        this.filled=filled;
        this.id=id;
        this.length = length;
        this.width = width;
    }
    
    public Rectangle(Color color,boolean filled,String id,int x, int y, int length, int width) {
    	this.color=color;
        this.filled=filled;
        this.id=id;
        this.x=x;
        this.y=y;
        this.length = length;
        this.width = width;
    }
    
    public Color getColor()
    {
    	return this.color;
    }
    
    public int getX()
    {
    	return this.x;
    }
    
    public int getY()
    {
    	return this.y;
    }
    
    public boolean isFilled()
    {
    	return this.filled;
    }
    
    public String getId()
    {
    	return this.id;
    }

    public void setColor(Color color)
    {
    	this.color=color;
    }
    
    public int getLength()
    {
    	return this.length;
    }
    
    public int getWidth()
    {
    	return this.width;
    }

    @Override
    public void draw(Graphics g) {
    	if(isFilled()==false)
    	{
    		System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
    		g.setColor(getColor());
    		g.drawRect(getX(),getY(),width,length);
    	}
    	else
    	{
    		System.out.println("Drawing a filled rectangel "+length+" "+getColor().toString());
    		g.setColor(getColor());
    		g.fillRect(getX(),getY(),width,length);
    	}
    }
}
