package g30126.Ciorba.Alexandru.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
	
	public int getLength();
	public int getWidth();
	public int getNumbricks();
	public int getLastNumBricks();
	public int getBricksInLayer();
    public abstract void draw(Graphics g);
    
}

