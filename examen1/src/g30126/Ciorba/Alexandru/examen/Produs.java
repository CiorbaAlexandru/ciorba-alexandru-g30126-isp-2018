package g30126.Ciorba.Alexandru.examen;

import java.awt.Graphics;

public interface Produs {

	int getPret();
	int getCantitate();
	void setCantitate(int cantitate);
	String getNume();
	public abstract void draw(Graphics g);
}
