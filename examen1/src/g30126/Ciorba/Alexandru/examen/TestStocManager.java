package g30126.Ciorba.Alexandru.examen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestStocManager {

	Produs p1;
	Produs p2;
	StocManager sm;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		p1= new ProduseCosmetice(50,5,200,"Ruj");
		p2= new ProduseAlimentare(150,5,200,"Paine");
		sm = new StocManager();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testadaugaProdus()
	{
		sm.adaugaProdus(p1);
		if(sm.getProduse().contains(p1)) {
			assertEquals(1,1);
		}
		else {
			assertEquals(1,2);
		}
			
	}
	
	@Test
	void teststergeProdus()
	{
		sm.adaugaProdus(p1);
		sm.adaugaProdus(p2);
		sm.stergeProdus(p2.getNume());
		if(sm.getProduse().contains(p2)) {
			assertEquals(1,2);
		}
		else {
			assertEquals(1,1);
		}
			
	}
	
	@Test
	void testafiseazaValoareStoc() {
		assertEquals(p1.getCantitate()*p1.getPret(),1000);
	}
	
	@Test
	void testafiseazaProdusCantitateMinima() {
		sm.afiseazaProdusCantitateMinima(300);
	}

	@Test
	void testincrementeazaStocProdus() {
		sm.incrementeazaStocProdus("Ruj");
		assertEquals(p1.getCantitate(),201);
	}
	
	@Test
	void testdecrementeazaStocProdus() {
		sm.decrementeazaStocProdus("Ruj");
		assertEquals(p1.getCantitate(),201);
	}
}
