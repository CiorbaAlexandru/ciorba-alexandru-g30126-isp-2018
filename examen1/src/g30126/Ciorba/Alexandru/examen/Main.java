package g30126.Ciorba.Alexandru.examen;

public class Main {

	public static void main(String[] args) {
		DrawingBoard b1 = new DrawingBoard();
		Produs p1= new ProduseCosmetice(50,5,100,"Ruj");
		b1.addShape(p1);
		Produs p2= new ProduseAlimentare(150,5,200,"Paine");
		b1.addShape(p2);
		Produs p3= new ProduseDidactice(250,5,300,"Caiet");
	    b1.addShape(p3);
	}
}
