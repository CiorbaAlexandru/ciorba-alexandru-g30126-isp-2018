package g30126.Ciorba.Alexandru.examen;

public interface IStocManager {

	 /**
	    * Adauga produs nou in stoc.
	    *
	    * @param p
	    */
	   void adaugaProdus(Produs p);

	   /**
	    * Sterge produs din stoc.
	    *
	    * @param numeProdus
	    */
	   void stergeProdus(String numeProdus);

	   /**
	    *private Afiseaza valoare totala a stocului: sum(prod.pret*prod.cantitate)
	    */
	   void afiseazaValoareStoc();

	   /**
	    * Afiseaza produsul cu cantitatea minima din stoc.
	    * @param min
	    */
	   void afiseazaProdusCantitateMinima(int min);

	   /**
	    * Incrementeaza cu o unitate cantitatea unui produs din stoc.
	    *
	    * @param numeProdus
	    */
	   void incrementeazaStocProdus(String numeProdus);

	   /**
	    * Decrementeaza cu o unitate cantitatea unui produs din stoc.
	    *
	    * @param numeProdus
	    */
	   void decrementeazaStocProdus(String numeProdus);
}
