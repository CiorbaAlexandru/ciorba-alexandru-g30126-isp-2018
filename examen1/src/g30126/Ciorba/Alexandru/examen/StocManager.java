package g30126.Ciorba.Alexandru.examen;

import java.util.ArrayList;

public class StocManager {

	private ArrayList<Produs> produse;
	
	public StocManager()
	{
		produse = new ArrayList();
	}
	
	public ArrayList<Produs> getProduse()
	{
		return this.produse;
	}
	
	void adaugaProdus(Produs p)
	{
		produse.add(p);
	}
	
	void stergeProdus(String numeProdus)
	{
		for(Produs p:produse) 
		{
			if(p.getNume().equals(numeProdus))
			{
				produse.remove(p);
			}
		}
	}
	
	void afiseazaValoareStoc()
	{
		int sum=0;
		for(Produs p:produse) 
		{
			sum=sum+p.getPret()*p.getCantitate();
		}
		System.out.println("Valoare totala a stocului este "+sum);
	}

	void afiseazaProdusCantitateMinima(int min)
	{
		System.out.println("Produsele cu cantitatea minima sunt:");
		for(Produs p:produse) 
		{
			if(p.getCantitate()<min)
				System.out.println(p.getNume()+" ");
				
		}
	}

	void incrementeazaStocProdus(String numeProdus)
	{
		Produs p2;
		for(Produs p:produse) 
		{
			if(p.getNume()==numeProdus)
			{
				/*
				if(p instanceof ProduseCosmetice)
				{
					p2=new ProduseCosmetice(p.getPret(),p.getCantitate()+1,p.getNume());
					produse.remove(p);
					produse.add(p2);
				}
				*/
				p.setCantitate(p.getCantitate()+1);
			}
		}
		
	}

	void decrementeazaStocProdus(String numeProdus)
	{
		for(Produs p:produse) 
		{
			if(p.getNume()==numeProdus)
			{
				p.setCantitate(p.getCantitate()-1);
			}
		}
	}
}
