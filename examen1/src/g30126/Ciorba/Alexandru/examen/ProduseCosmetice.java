package g30126.Ciorba.Alexandru.examen;

import java.awt.Graphics;

public class ProduseCosmetice implements Produs {

	int pret;
	int cantitate;
	String nume;
	int x;
	
	public ProduseCosmetice(int x,int pret,int cantitate,String nume)
	{
		this.x=x;
		this.pret=pret;
		this.cantitate=cantitate;
		this.nume=nume;
	}
	
	public int getPret()
	{
		return this.pret;
	}
	
	public int getCantitate()
	{
		return this.cantitate;
	}
	
	public String getNume()
	{
		return this.nume;
	}
	
	public void setCantitate(int cantitate)
	{
		this.cantitate=cantitate;
	}
	
	public void draw(Graphics g)
	{
		g.fillRect(x,500,50,getCantitate());
		g.drawString(getNume(), x, 50);
	}
}
