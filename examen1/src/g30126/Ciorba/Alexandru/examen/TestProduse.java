package g30126.Ciorba.Alexandru.examen;

import static org.junit.Assert.assertEquals;


import java.awt.Color;

import org.junit.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;



class TestProduse {

	Produs p1;
	Produs p2;
	Produs p3;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		p1= new ProduseCosmetice(50,5,200,"Ruj");
		p2= new ProduseAlimentare(150,5,200,"Paine");
		p3= new ProduseDidactice(250,5,200,"Caiet");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testgetPret() {
		assertEquals(p1.getPret(),5);
	}
	
	@Test
	void testgetCantitate() {
		assertEquals(p1.getCantitate(),200);
	}
	
	@Test
	void testgetNume() {
		/*
		if(p1.getNume().equals("Ruj"))
		{
			assertEquals(1,1);
		}
		else
		{
			assertEquals(2,1);
		}
		*/
		assertEquals(p1.getNume(),"Ruj");
	}
	
	@Test
	void testdraw() {
		DrawingBoard b1 = new DrawingBoard();
        b1.addShape(p1);
        b1.addShape(p2);
        b1.addShape(p3);
	}

}
