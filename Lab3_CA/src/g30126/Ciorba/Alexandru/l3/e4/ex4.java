package g30126.Ciorba.Alexandru.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class ex4
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 3, 2, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 4, 2, Direction.WEST);
      Wall blockAve2 = new Wall(ny, 3, 2, Direction.NORTH);
      Wall blockAve3 = new Wall(ny, 3, 3, Direction.NORTH);
      Wall blockAve4 = new Wall(ny, 4, 2, Direction.SOUTH);
      Wall blockAve5 = new Wall(ny, 4, 3, Direction.SOUTH);
      Wall blockAve6 = new Wall(ny, 3, 3, Direction.EAST);
      Wall blockAve7 = new Wall(ny, 4, 3, Direction.EAST);
      Robot mark = new Robot(ny, 2, 4, Direction.WEST);
 
 
      // mark goes around the roadblock
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
     
   }
}
