package g30126.Ciorba.Alexandru.l3.e3;

import becker.robots.*;

public class ex3
{
   public static void main(String[] args)
   {  
   	// Set up the initial situation
   	City prague = new City();
   	Thing parcel = new Thing(prague, 2, 2);
      Robot karel = new Robot(prague, 5, 5, Direction.NORTH);
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
   }
} 