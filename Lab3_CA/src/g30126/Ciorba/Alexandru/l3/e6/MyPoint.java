package g30126.Ciorba.Alexandru.l3.e6;

import becker.robots.*;
import java.util.Scanner;

public class MyPoint
{
	private int x,y;
	public MyPoint()
	{
		x=0;
		y=0;
	}
	
	public MyPoint(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void setX(int x)
	{
		this.x=x;	
	}
	
	public void setY(int y)
	{
		this.y=y;	
	}
	
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;	
	}
	
	public String toString()
	{
		return "("+this.x+","+this.y+")"; 
	}
	
	public double distance(int x,int y)
	{
		return Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
	}
	
	public double distance(MyPoint another)
	{
		return Math.sqrt((this.x-another.x)*(this.x-another.x)+(this.y-another.y)*(this.y-another.y));
	}

} 
