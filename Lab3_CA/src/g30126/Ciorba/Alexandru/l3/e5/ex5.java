package g30126.Ciorba.Alexandru.l3.e5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class ex5
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Thing parcel = new Thing(ny, 2, 2);
      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve4 = new Wall(ny, 2, 1, Direction.SOUTH);
      Wall blockAve5 = new Wall(ny, 1, 2, Direction.SOUTH);
      Wall blockAve6 = new Wall(ny, 1, 2, Direction.EAST);

      Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
 
 
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.pickThing();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
   }
}