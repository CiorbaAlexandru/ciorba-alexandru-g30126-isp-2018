package examen_1;
//factory
abstract class Senzor2 {
	
	public String locatie;
	
	public Senzor2()
	{    
		
	}
	
	public void afis() {
		System.out.println(this.locatie);
	}
	
	public void setLocatie()
	{    
		this.locatie="Cluj";
	}		
}

class A extends Senzor2
{
	public void setLocatie()
	{    
		this.locatie="Oradea";
	}
}

class B extends Senzor2
{
	public void setLocatie()
	{    
		this.locatie="Cluj";
	}
}

class SenzorType
{
	public Senzor2 createSenzor(String type)
	{
		if(type.equals("A"))
		{
			return new A();
		}
		else
		{
			return new B();
		}
	}
}
