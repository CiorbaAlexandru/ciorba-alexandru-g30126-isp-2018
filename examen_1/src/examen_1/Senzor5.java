package examen_1;

public class Senzor5 implements Senzor5b{
public String senzori[]= {"ORadea","Cluj","Andrei"};

public Senzor5a getIterator() {
	return new NameIterator();
}

private class NameIterator implements Senzor5a{
	int index;
	
	@Override
	public boolean hasNext() {
	return index < senzori.length;
	}
	
	@Override
	public Object next() {
		if(this.hasNext()) {
			return senzori[index++];
			
		}
		return null;
	}
}
}