package examen_1;

public class Senzor6a {
	    public static void main(String[] args) throws InterruptedException{
	        Alarm fireAlarm = new Alarm();
	        AlarmMonitor monitor = new AlarmMonitor();
	        fireAlarm.register(monitor);
	 
	        fireAlarm.startAlarm();
	        Thread.sleep(500);
	        fireAlarm.stopAlarm();
	 
	    }
}
