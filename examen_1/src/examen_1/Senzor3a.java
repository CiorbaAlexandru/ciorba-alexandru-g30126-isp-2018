package examen_1;

interface Senzor3a {
	void a();
	void b();
}

class locatiiReale implements Senzor3a
{
	public void a()
	{
		System.out.println("Oradea");
	}
	public void b()
	{
		System.out.println("Cluj");
	}
}

class Proxy implements Senzor3a
{
	private locatiiReale real;
	Proxy()
	{
		real= new locatiiReale();
	}
	
	@Override
	public void a()
	{
		real.a();
	}
	@Override
	public void b()
	{
		real.b();
	}
}