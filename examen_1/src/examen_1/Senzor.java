package examen_1;
//singleton
public class Senzor
{
	String locatie;
	private static Senzor log;
	
	private Senzor(String locatie)
	{
		this.locatie=locatie;
	}
	
	public static Senzor getSenzor(String locatie)
	{
		if(log==null)
			log=new Senzor(locatie);
		return log;
	}
	
	public static void main(String[] args)
	{
		Senzor a=getSenzor("Oradea");
		Senzor b=getSenzor("Oradea");
		System.out.println(a.locatie);
		System.out.println(b.locatie);
	}	
}