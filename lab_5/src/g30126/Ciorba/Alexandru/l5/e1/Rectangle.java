package g30126.Ciorba.Alexandru.l5.e1;

public class Rectangle extends Shape{
	protected double width;
	protected double length;
	
	public Rectangle()
	{
		super();
		this.width=2;
		this.length=3;
	}
	
	public Rectangle(double width,double length)
	{
		super();
		this.width=width;
		this.length=length;
	}
	
	public Rectangle(double width,double length,String color,boolean filled)
	{
		super(color,filled);
		this.width=width;
		this.length=length;
		
	}
	
	public double getWidth()
	{
		return this.width;
	}
	
	public void setWidth(double width)
	{
		this.width=width;
	}
	
	public double getLength()
	{
		return this.length;
	}
	
	public void setLength(double length)
	{
		this.length=length;
	}
	
	public double getArea()
	{
		return this.length*this.width;
	}
	
	public double getPerimeter()
	{
		return 2*this.length+2*this.width;
	}
	
	public String toString() {
	       return "Rectangle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';
	   }
}
