package g30126.Ciorba.Alexandru.l5.e1;

public class Test {
	public static void main(String [] args)
	{
		Shape [] x = new Shape[4];
		//Shape a = new Shape();
		Circle b = new Circle();
		Rectangle c = new Rectangle();
		Square d = new Square(2);
		x[1]=b;
		x[2]=c;
		x[3]=d;
		System.out.println("Shape 1 area="+x[1].getArea()+",Shape 1 perimeter="+x[1].getPerimeter());
		System.out.println("Shape 2 area="+x[2].getArea()+",Shape 2 perimeter="+x[2].getPerimeter());
		System.out.println("Shape 3 area="+x[3].getArea()+",Shape 3 perimeter="+x[3].getPerimeter());
	}
}
