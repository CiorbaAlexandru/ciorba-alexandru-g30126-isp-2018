package g30126.Ciorba.Alexandru.l5.e1;

public class Square extends Rectangle{
	public Square()
	{
		super();
	}
	
	public Square(double side)
	{
		super(side,side);
	}
	
	public Square(double side,String color,boolean filled)
	{
		super(side,side,color,filled);
	}
	
	public double getSide()
	{
		return this.width;
	}
	
	public void setSide(double side)
	{
		this.width=this.length=side;
	}
	
	public void setWidth(double side)
	{
		this.width=this.length=side;
	}
	
	public void setLength(double side)
	{
		this.width=this.length=side;
	}
	
	public String toString() {
	       return "Square{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';
	   }
}
