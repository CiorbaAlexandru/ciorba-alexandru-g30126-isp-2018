package g30126.Ciorba.Alexandru.l5.e1;

public class Circle extends Shape{
	protected double radius;
	
	public Circle()
	{
		super();
		this.radius=2;
	}
	
	public Circle(double radius)
	{
		super();
		this.radius=radius;
	}
	
	public Circle(double radius,String color,boolean filled)
	{
		super(color,filled);
		this.radius=radius;
	}
	
	public double getRadius()
	{
		return this.radius;
	}
	
	public void setRadius(double radius)
	{
		this.radius=radius;
	}
	
	public double getArea()
	{
		return 3.14*this.radius*this.radius;
	}
	
	public double getPerimeter()
	{
		return 2*3.14*this.radius;
	}
	
	public String toString() {
	       return "Circle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';
	   }
}
