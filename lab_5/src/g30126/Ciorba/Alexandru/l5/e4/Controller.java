package g30126.Ciorba.Alexandru.l5.e4;

import java.util.Timer;

public class Controller {
	public static void control()
	{
		TemperatureSensor a;
		a=TemperatureSensor.getTemperatureSensor();
		LightSensor b;
		b=LightSensor.getLightSensor();
		
		int c,d;
		int i;
		for(i=0;i<20;i++)
		{
			try
			{
				Thread.sleep(1000);
				i++;
				c=a.readValue();
				d=b.readValue();
				System.out.println("TemperatureSensor value="+c);
				System.out.println("LightSensor value="+d);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String [] args) throws InterruptedException
	{
		control();
	}
}
