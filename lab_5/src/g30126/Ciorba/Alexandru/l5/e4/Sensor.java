package g30126.Ciorba.Alexandru.l5.e4;

public abstract class Sensor {
	
	private String location;
	
	abstract int readValue();
	
	
	public String getLocation()
	{
		return this.location;
	}
}
