package g30126.Ciorba.Alexandru.l5.e4;

import java.util.Random;

public class LightSensor extends Sensor{
	
	private static LightSensor ls;
	
	private LightSensor()
	{
		
	}
	
	public static LightSensor getLightSensor()
	{
		if(ls==null) {
			ls = new LightSensor(); 
		}
		return ls;
	}
	public int readValue()
	{
		Random r = new Random();
		int n=r.nextInt(100);
		return n;
	}
}
