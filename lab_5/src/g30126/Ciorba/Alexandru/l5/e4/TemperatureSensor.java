package g30126.Ciorba.Alexandru.l5.e4;

import java.util.Random;

public class TemperatureSensor extends Sensor{
	
	private static TemperatureSensor ts;
	
	private TemperatureSensor()
	{
		
	}
	
	public static TemperatureSensor getTemperatureSensor() {
		if(ts==null) {
			ts = new TemperatureSensor();
		}
		return ts;
	}
	
	public int readValue()
	{
		Random r = new Random();
		int n=r.nextInt(100);
		return n;
	}
}
