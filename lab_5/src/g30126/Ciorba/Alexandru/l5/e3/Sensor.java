package g30126.Ciorba.Alexandru.l5.e3;

public abstract class Sensor {
	private String location;
	public abstract int readValue();
	public String getLocation()
	{
		return this.location;
	}
}
