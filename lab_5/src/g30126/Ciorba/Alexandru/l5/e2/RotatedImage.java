package g30126.Ciorba.Alexandru.l5.e2;

public class RotatedImage implements Image{
	private String fileName;
	 
	   public RotatedImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   public void display() {
	      System.out.println("Displaying Rotated " + fileName);
	   }
	 
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
}
