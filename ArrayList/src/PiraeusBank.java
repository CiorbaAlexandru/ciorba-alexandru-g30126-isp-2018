import java.util.ArrayList;
import java.util.Collections;

public class PiraeusBank {

	ArrayList<BankAccount> accounts = new ArrayList<>();
	
	
	public void addAcount(String owner, double balance) {
		BankAccount ba = new BankAccount(owner,balance);
		accounts.add(ba);
	}
	
	public void printAccounts() {
		Collections.sort(accounts);
		for(BankAccount a:accounts) {
		
			System.out.println(a);
		}
	}
	
	public void printAccounts(double minBalance,double maxBalance) {
		for(BankAccount a:accounts) {
			if(a.getBalance()>minBalance && a.getBalance() <maxBalance) {
				System.out.println(a);
			}
		}
	}
		public BankAccount getAccount(String owner){
			for(BankAccount a:accounts) {
				if(a.getOwner()==owner) {
					return a;
				}
			}
			return null;
		}

		
		
		public static void main(String args[]) {
			PiraeusBank scridon  = new PiraeusBank();
			scridon.addAcount("anreiy", 40);
			scridon.addAcount("deyu", 38);
			scridon.addAcount("zanreiy", 20);
			scridon.addAcount("iiiianreiy", 66);
			scridon.addAcount("ioneL", 409);
			
			scridon.printAccounts();
			
			scridon.printAccounts(1, 41);
			
			scridon.getAccount("ioneL");
			
		}
		
	}
	


