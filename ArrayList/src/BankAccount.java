
public class BankAccount implements Comparable<BankAccount>{

	private String owner;
	private double balance;

	public double getBalance() {
		return balance;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public BankAccount(String o,double b) {
		this.owner=o;
		this.balance=b;
	}
	
	public void withdraw(double amount) {
		balance = balance - amount;
	}
	
	public void deposit(double amount) {
		balance = balance + amount;
	}
	
	public int compareTo(BankAccount b) {
        return this.owner.compareTo(b.owner);
  
}

	
	public String toString() {
		return "Owner: " + getOwner() + " balance: " + getBalance();
	}
}
